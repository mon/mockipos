NAME = mockipos
VERSION = 0

default:

install:
	install -D mockipos $(DESTDIR)/usr/bin/mockipos
	install -d $(DESTDIR)/etc/mock/
	cp -n etc/mockipos.conf $(DESTDIR)/etc/
	cp -n etc/mock/*.cfg $(DESTDIR)/etc/mock/

archive:
	@git archive --prefix=$(NAME)-$(VERSION)/ HEAD --format=tar.gz -o $(NAME)-$(VERSION).tar.gz
	@echo "$(NAME)-$(VERSION).tar.gz created"

clean:
	rm -f *~ *tar.gz
